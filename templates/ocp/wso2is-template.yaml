apiVersion: v1
kind: Template
labels:
  template: wso2is
  version: 5.7.0
  app: wso2is
metadata:
  name: wso2is
objects:
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      app: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    selector:
      app: ${APPLICATION_NAME}
      deploymentconfig: ${APPLICATION_NAME}
    replicas: 1
    template:
      metadata:
        labels:
          app: ${APPLICATION_NAME}
          deploymentconfig: ${APPLICATION_NAME}
      spec:
        serviceAccountName: ${APPLICATION_NAME}
        hostAliases:
        - ip: "127.0.0.1"
          hostnames:
          - "wso2is-wso2.${DOMAIN}"
        containers:
        - image: chakraymx/wso2is-ocp:5.7.0
          imagePullPolicy: Always
          name: ${APPLICATION_NAME}
          
          volumeMounts:
          - name: identity-server-conf
            mountPath: /home/wso2usr/config/repository/conf
          - name: identity-server-conf-axis2
            mountPath: /home/wso2usr/config/repository/conf/axis2
          - name: identity-server-conf-datasources
            mountPath: /home/wso2usr/config/repository/conf/datasources
          - name: identity-server-conf-tomcat
            mountPath: /home/wso2usr/config/repository/conf/tomcat
          - name: identity-server-conf-identity
            mountPath: /home/wso2usr/config/repository/conf/identity
          - name: identity-server-conf-event-publishers
            mountPath: /home/wso2usr/config-server/eventpublishers
          - name: shared-deployment-persistent-disk
            mountPath: /home/wso2usr/server
          - name: shared-tenants-persistent-disk
            mountPath: /home/wso2usr/wso2is-5.7.0/repository/tenants
        volumes:
        - name: identity-server-conf
          configMap:
            name: identity-server-conf
        - name: identity-server-conf-axis2
          configMap:
            name: identity-server-conf-axis2
        - name: identity-server-conf-datasources
          configMap:
            name: identity-server-conf-datasources
        - name: identity-server-conf-identity
          configMap:
            name: identity-server-conf-identity
        - name: identity-server-conf-tomcat
          configMap:
            name: identity-server-conf-tomcat
        - name: identity-server-conf-event-publishers
          configMap:
            name: identity-server-conf-event-publishers
        - name: shared-deployment-persistent-disk
          persistentVolumeClaim:
            claimName: is-shared-deployments
        - name: shared-tenants-persistent-disk
          persistentVolumeClaim:
            claimName: is-shared-tenants
    triggers:
    - imageChangeParams:
        automatic: true
        containerNames:
        - ${APPLICATION_NAME}
        from:
          kind: ImageStreamTag
          name: ${APPLICATION_NAME}:5.7.0
      type: ImageChange
    - type: ConfigChange
- apiVersion: v1
  kind: ImageStream
  metadata:
    name: wso2is
  spec:
    dockerImageRepository: chakraymx/wso2is-ocp:5.7.0
- apiVersion: v1
  kind: ServiceAccount
  metadata:
    creationTimestamp: null
    labels:
      template: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
- apiVersion: v1
  kind: Service
  metadata:
    label:
      app: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    ports:
    - name: servlet-http
      port: 9763
      targetPort: 9763
      protocol: TCP
    - name: servlet-https
      port: 9443
      targetPort: 9443
      protocol: TCP
    selector:
      deploymentconfig: ${APPLICATION_NAME}
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: is-shared-tenants
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: ${TENANTS_VOLUME_CAPACITY}
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: is-shared-deployments
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: ${DEPLOYMENTS_VOLUME_CAPACITY}
- apiVersion: v1
  kind: Route
  metadata:
    namespace: wso2
    label:
      app: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    port:
      targetPort: servlet-https
    tls:
      termination: passthrough
    to:
      kind: Service
      name: ${APPLICATION_NAME}

- apiVersion: v1
  kind: Secret
  metadata:
    annotations:
      template.openshift.io/expose-database_name: "{.data['database-name']}"
      template.openshift.io/expose-password: "{.data['database-password']}"
      template.openshift.io/expose-root_password: "{.data['database-root-password']}"
      template.openshift.io/expose-username: "{.data['database-user']}"
    name: "${DATABASE_SERVICE_NAME}"
  stringData:
    database-name: "${MYSQL_DATABASE}"
    database-password: "${MYSQL_PASSWORD}"
    database-root-password: "${MYSQL_ROOT_PASSWORD}"
    database-user: "${MYSQL_USER}"
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      template.openshift.io/expose-uri: mysql://{.spec.clusterIP}:{.spec.ports[?(.name=="mysql")].port}
    name: "${DATABASE_SERVICE_NAME}"
  spec:
    ports:
    - name: mysql
      port: 3306
    selector:
      name: "${DATABASE_SERVICE_NAME}"
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: "${DATABASE_SERVICE_NAME}"
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: "${VOLUME_CAPACITY}"
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    annotations:
      template.alpha.openshift.io/wait-for-ready: 'true'
    name: "${DATABASE_SERVICE_NAME}"
  spec:
    replicas: 1
    selector:
      name: "${DATABASE_SERVICE_NAME}"
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: "${DATABASE_SERVICE_NAME}"
      spec:
        containers:
        - env:
          - name: MYSQL_USER
            valueFrom:
              secretKeyRef:
                key: database-user
                name: "${DATABASE_SERVICE_NAME}"
          - name: MYSQL_PASSWORD
            valueFrom:
              secretKeyRef:
                key: database-password
                name: "${DATABASE_SERVICE_NAME}"
          - name: MYSQL_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                key: database-root-password
                name: "${DATABASE_SERVICE_NAME}"
          - name: MYSQL_DATABASE
            valueFrom:
              secretKeyRef:
                key: database-name
                name: "${DATABASE_SERVICE_NAME}"
          image: "mysql:5.7"
          imagePullPolicy: Always
          livenessProbe:
            initialDelaySeconds: 30
            tcpSocket:
              port: 3306
            timeoutSeconds: 1
          name: mysql
          ports:
          - containerPort: 3306
          readinessProbe:
            exec:
              command:
              - "/bin/sh"
              - "-i"
              - "-c"
              - MYSQL_PWD="$MYSQL_PASSWORD" mysql -h 127.0.0.1 -u $MYSQL_USER -D $MYSQL_DATABASE
                -e 'SELECT 1'
            initialDelaySeconds: 5
            timeoutSeconds: 1
          resources:
            limits:
              memory: "${MEMORY_LIMIT}"
          volumeMounts:
          - name: "mysql-dbscripts"
            mountPath: /docker-entrypoint-initdb.d
          - mountPath: "/var/lib/mysql/data"
            name: "${DATABASE_SERVICE_NAME}-data"
        volumes:
        - name: mysql-dbscripts
          configMap:
            name: mysql-dbscripts
        - name: "${DATABASE_SERVICE_NAME}-data"
          persistentVolumeClaim:
            claimName: "${DATABASE_SERVICE_NAME}"
    triggers:
    - imageChangeParams:
        automatic: true
        containerNames:
        - mysql
        from:
          kind: ImageStreamTag
          name: mysql:${MYSQL_VERSION}
          namespace: "${NAMESPACE}"
      type: ImageChange
    - type: ConfigChange
- apiVersion: v1
  kind: ImageStream
  metadata:
    name: mysql
  spec:
    dockerImageRepository: mysql:5.7.0
parameters:
- name: DOMAIN
  required: true
  value: {{ domain }}
- name: APPLICATION_NAME
  required: true
  value: wso2is
- description: Volume space available for deployments, e.g. 512Mi, 2Gi
  name: DEPLOYMENTS_VOLUME_CAPACITY
  required: true
  value: 1Gi
- description: Volume space available for tenants, e.g. 512Mi, 2Gi
  name: TENANTS_VOLUME_CAPACITY
  required: true
  value: 1Gi
- description: Maximum amount of memory the container can use.
  displayName: Memory Limit
  name: MEMORY_LIMIT
  required: true
  value: 512Mi
- description: The OpenShift Namespace where the ImageStream resides.
  displayName: Namespace
  name: NAMESPACE
  value: openshift
- description: The name of the OpenShift Service exposed for the database.
  displayName: Database Service Name
  name: DATABASE_SERVICE_NAME
  required: true
  value: mysql
- description: Username for MySQL user that will be used for accessing the database.
  displayName: MySQL Connection Username
  value: wso2carbon
  name: MYSQL_USER
  required: true
- description: Password for the MySQL connection user.
  displayName: MySQL Connection Password
  value: wso2carbon
  name: MYSQL_PASSWORD
  required: true
- description: Password for the MySQL root user.
  displayName: MySQL root user Password
  value: wso2root
  name: MYSQL_ROOT_PASSWORD
  required: true
- description: Name of the MySQL database accessed.
  displayName: MySQL Database Name
  name: MYSQL_DATABASE
  required: true
  value: sampledb
- description: Volume space available for data, e.g. 512Mi, 2Gi.
  displayName: Volume Capacity
  name: VOLUME_CAPACITY
  required: true
  value: 10Gi
- description: Version of MySQL image to be used (5.7, or latest).
  displayName: Version of MySQL Image
  name: MYSQL_VERSION
  required: true
  value: '5.7'  
  
