## Install WSO2 Identity Server using Ansible

link:https://www.ansible.com/[Ansible] can be used to automate the installation process to setup cicd tools in Openshift.

Make sure you have the following requirements installed:

- Git
- Ansible 1.9+

And run the following commands:

``
$ git clone https://ADemesaCkry@bitbucket.org/ADemesaCkry/ocp-wso2is.git
``

``
$ cd ocp-wso2is/
``

First, *edit the vars.yaml file* to define the *'openshift_console'*, *'domain'* variables and set the admin console and provide subdomain for your openshift installation.

Then execute:

``
$ ansible-playbook wso2is.yaml
``

The playbook install the next tools:

- WSO2 Identity Server 5.7
- MySQL as product database
