#!/bin/bash

CARBON_HOST="*"
KEYSTORE_ALIAS=wso2carbon

DNAME="CN=$CARBON_HOST.$DOMAIN,OU=Developer,O=Chakray,ST=Sevilla,C=ES"
KEYSTORE_FILE=wso2carbon.jks
KEYSTORE_PASSWORD=wso2carbon
P12_FILE=wso2carbon.p12
EXTENSIONS="digitalSignature,keyEncipherment,dataEncipherment"

rm wso2carbon.*	
rm client-truststore.jks 	

keytool -genkey -noprompt -alias $KEYSTORE_ALIAS -ext KeyUsage=$EXTENSIONS \
    -keyalg RSA -sigalg SHA256withRSA -keysize 2048 -validity 3650 -dname "$DNAME" \
    -keystore $KEYSTORE_FILE -storepass $KEYSTORE_PASSWORD -keypass $KEYSTORE_PASSWORD

keytool -export -noprompt -alias $KEYSTORE_ALIAS -file $KEYSTORE_ALIAS.crt \
    -keystore $KEYSTORE_FILE -storepass $KEYSTORE_PASSWORD -keypass $KEYSTORE_PASSWORD
cp client-truststore-complete.jks client-truststore.jks 

keytool -import -noprompt -alias $KEYSTORE_ALIAS -file $KEYSTORE_ALIAS.crt \
    -keystore client-truststore.jks -storepass wso2carbon -keypass $KEYSTORE_PASSWORD

rm wso2carbon.crt
chmod 750 *.jks
